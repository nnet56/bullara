#!/bin/sh

# This script facilitates the visualization of the data generated

cd C/
make

# echo "Generating matrices..."
./bullara
./bullara --irid1
./bullara --irid10

# cd ../R/
# echo "Generating graphics..."
# Rscript graphics.R

cd ../scilab/
echo "Generating graphics..."
scilab -nw -f graphs.sce

cd ../
echo "Launching browser"
firefox graph.html &
