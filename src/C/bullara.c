/* ---
 * Marc MELKONIAN
 * M2 Bioinformatique (IBI) - Université Rennes 1
 * BS1 - Simulation des systèmes biologiques
 * Creation date: 02/10/2019
 *
 * This present script aims to reproduce the two figures (Fig. 1, Fig. 2) of an
 * article by Bullara(1) simulating the generation of the striped patterns of
 * the zebrafish skin by a Monte Carlo simulation of a Reaction-Diffusion
 * mechanism.
 *
 * (1) Bullara D, De Decker Y. Pigment cell movement is not required for
 * generation of Turing patterns in zebrafish skin. Nat Commun. 2015 May
 * 11;6:6971. doi: 10.1038/ncomms7971. PubMed PMID: 25959141; PubMed Central
 * PMCID: PMC4432648.
 * ---
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>

#define sz(x) ( sizeof(x)/sizeof(x[0]) )


// global vars ~~~
int SIDE = 100;  // lattice is a 100x100 square
int MAX_IDX;     // maximum index on the lattice (optimization purpose)
int RUNS = 1e8;  // nb of iterations for one Monte Carlo simulation

// chromatophores ids
// we start with 1 for compatibility with scilab color maps
enum Chromatophores_ids {
        SEMATOPHORE_ID = 1,
        MELANOPHORE_ID = 2,
        XANTOPHORE_ID = 3,
        IRIDOPHORE_ID = 4
};

// changing states interactions
typedef enum {
	INTER_BX = 0,
	INTER_SM,
	INTER_SX,
	INTER_BM,
	INTER_DX,
	INTER_DM,
	INTER_LX
} Interaction_t;

// interactions rates
float BX_RATE = 1.f;
float BM_RATE = 0.f;
float DX_RATE = 0.f;
float DM_RATE = 0.f;
float SX_RATE = 1.f;
float SM_RATE = 1.f;

// interactions types
Interaction_t INTERACTIONS[7] = {
	INTER_BX, INTER_SM, INTER_SX, INTER_BM, INTER_DX, INTER_DM, INTER_LX
};
// Interaction_t INTERACTIONS[4] = {
//         INTER_BX, INTER_SX, INTER_SM, INTER_LX
// };

typedef enum {
        IRID_ZERO = 0,
        IRID_ONE,
        IRID_TEN
} Irid_st_id_t ;
Irid_st_id_t IRID_ST = IRID_ZERO;

// probabilities distribution
typedef struct cdf_t {
	float* probs;  // distribution of probs
	float max;     // sum of probs (should be equal to 1.f)
	int len;       // number of probs
} cdf_t;

// export directories
char* EXPORTS_DIR = "../exports/matrices/";
struct stat st = {0};

// a node identifies a (x, y) position on the lattice
typedef struct node {
	int x;
	int y;
} node;


// functions ~~~

// helper function to print a memory allocation error and exit with a given
// code
void
memerr(char* msg, int errcode)
{
	fprintf(stderr, "Error: cannot malloc %s\n", msg);
	exit(errcode);
}

// print lattice to stdout
void
dump_lattice(int** lattice)
{
	for (int i = 0; i < SIDE; ++i) {
		for (int j = 0; j < SIDE; ++j) {
			printf("%d ", lattice[i][j]);
		}
		printf("\n");
	}
}

// reinitialize lattice with only sematophores
void
reset_lattice(int** lattice)
{
	for (int i = 0; i < SIDE; ++i) {
		for (int j = 0; j < SIDE; ++j) {
			lattice[i][j] = SEMATOPHORE_ID;
		}
	}
}

// get first neighbors of a node
// each node has a coordinancy of 4, so the first neighbors of the node *
// are defined by the nodes a, b, c, d as below:
// +-+-+-+-+-+-+
// | | |d| | | |
// +-+-+-+-+-+-+
// | |a|*|c| | |
// +-+-+-+-+-+-+
// | | |b| | | |
// +-+-+-+-+-+-+
node*
m_pick_rd_snode(int x, int y)
{
	// pick one random offset
	int n = rand() % 4;
        switch (n) {
                case 0: x += 1; break;
                case 1: x -= 1; break;
                case 2: y += 1; break;
                case 3: y -= 1; break;
        }

	// avoid out of bounds coordinates with periodic shifts
        if (x > MAX_IDX)  { x = 0; }
        else if (x < 0)   { x = MAX_IDX; }
        if (y > MAX_IDX)  { y = 0; }
        else if (y < 0)   { y = MAX_IDX; }

	node* nd = malloc(sizeof(node));
	if ( ! nd ) {
                memerr("node", 1);
	}
	nd->x = x;
	nd->y = y;
	return nd;
}

// pick a random distant node located at a given distance
node*
m_pick_rd_lnode(int x, int y, float h)
{
	float angle = ((float)rand() / (float)RAND_MAX) * 2 * M_PI;
	float a = x + h * cosf(angle);
	float b = y + h * sinf(angle);

	// avoid out of bounds coordinates
	if (a > MAX_IDX) { a -= SIDE; }
	else if (a < 0)  { a += SIDE; }
	if (b > MAX_IDX) { b -= SIDE; }
	else if (b < 0)  { b += SIDE; }

	node* nd = malloc(sizeof(node));
	if ( ! nd ) {
		memerr("node", 1);
	}
	nd->x = (int)a;
	nd->y = (int)b;
	return nd;
}

// Si -> Xi (birth of xantophores)
void
bx_mod(int** L, int x, int y)
{
	if ( L[x][y] == SEMATOPHORE_ID ) {
		L[x][y] = XANTOPHORE_ID;
	}
}

// Si -> Mi (birth of melanophores)
void
bm_mod(int** L, int** IRID, int x, int y)
{
	if ( L[x][y] == SEMATOPHORE_ID
		&& IRID[x][y] == SEMATOPHORE_ID ) {
		L[x][y] = MELANOPHORE_ID;
	}
}

// Xi -> Si (death of xantophores)
void
dx_mod(int** L, int x, int y)
{
	if ( L[x][y] == XANTOPHORE_ID ) {
		L[x][y] = SEMATOPHORE_ID;
	}
}

// Mi -> Si (death of melanophores)
void
dm_mod(int** L, int x, int y)
{
	if ( L[x][y] == MELANOPHORE_ID ) {
		L[x][y] = SEMATOPHORE_ID;
	}
}

// Mi+/-1 + Xi -> Mi+/-1 + Si
// (short range melanphore-induced death of xantophores)
void
sm_mod(int** L, int x, int y)
{
	if ( L[x][y] != XANTOPHORE_ID ) {
                return;
        }

        node* snode = m_pick_rd_snode(x, y);
        if ( L[snode->x][snode->y] == MELANOPHORE_ID ) {
                L[x][y] = SEMATOPHORE_ID;
	}
        free(snode);
}

// Xi+/-1 + Mi -> Xi+/-1 + Si
// (short range xantophore-induced death of melanophores)
void
sx_mod(int** L, int x, int y)
{
	if ( L[x][y] != MELANOPHORE_ID ) {
                return;
        }

        node* snode = m_pick_rd_snode(x, y);
        if ( L[snode->x][snode->y] == XANTOPHORE_ID ) {
                L[x][y] = SEMATOPHORE_ID;
        }
        free(snode);
}

// Xi+/-h + Si -> Xi+/-h + Mi
// (long range xantophore-induced birth of melanophores)
void
lx_mod(int** L, int** IRID, int x, int y, float h)
{
        if ( L[x][y] != SEMATOPHORE_ID
		|| IRID[x][y] != SEMATOPHORE_ID ) {
                return;
        }

        node* lnode = m_pick_rd_lnode(x, y, h);
        if ( L[lnode->x][lnode->y] == XANTOPHORE_ID ) {
                L[x][y] = MELANOPHORE_ID;
        }
        free(lnode);
}

void
interact(int** L, int** IRID, float h, int x, int y, Interaction_t evt)
{
	// update the lattice with the selected interaction implying the given
	// node coordinates
	switch (evt) {
                case INTER_BX: { bx_mod(L, x, y);          } break;
                case INTER_BM: { bm_mod(L, IRID, x, y);    } break;
                case INTER_DX: { dx_mod(L, x, y);          } break;
                case INTER_DM: { dm_mod(L, x, y);          } break;
                case INTER_SM: { sm_mod(L, x, y);          } break;
                case INTER_SX: { sx_mod(L, x, y);          } break;
                case INTER_LX: { lx_mod(L, IRID, x, y, h); } break;
           	default: break;
        }
}


// cumulative distribution of events frequencies
cdf_t*
m_init_cdf(float lx)
{
	cdf_t * cdf = malloc(sizeof(cdf));
	if ( ! cdf ) {
		memerr("cdf", 1);
	}

	int n = sz(INTERACTIONS);
	float* probs = malloc(sizeof(float) * n);
	if ( ! probs ) {
		memerr("probs", 1);
	}

        float acc = 0;
	for (int i = 0; i < n; ++i) {
                switch (INTERACTIONS[i]) {
                        case INTER_BX: { acc += BX_RATE; } break;
                        case INTER_BM: { acc += BM_RATE; } break;
                        case INTER_DX: { acc += DX_RATE; } break;
                        case INTER_DM: { acc += DM_RATE; } break;
                        case INTER_SX: { acc += SX_RATE; } break;
                        case INTER_SM: { acc += SM_RATE; } break;
                        case INTER_LX: { acc += lx;      } break;
                }
		probs[i] = acc;
	}

	cdf->probs = probs;
	cdf->max = acc;
	cdf->len = n;
	return cdf;
}

// pick a random interaction with respect to the cumulated probabilities
// frequencies distribution of all the events
//
// The idea is to sum all the probs and to pick a random number between 0 and
// this sum.
// Example for bx = sm = sx = 1 and lx = 0.5:
// +----------+----------+---------+-----+
// 0          1          2         3    3.5
// Then, by iterating through the probabilities array, we can find the index of
// the associated event.
Interaction_t
pick_rd_interaction(cdf_t* cdf)
{
	float x = ((float)rand() / (float)RAND_MAX) * cdf->max;
	int i = 0;
        for (i = 0; i < cdf->len ; ++i) {
                if ( x <= cdf->probs[i] ) {
                        break;
		}
	}
	return INTERACTIONS[i];
}

// save lattice to file
void
save_lattice(int** L, float lx, float h)
{
        if (stat(EXPORTS_DIR, &st) == -1) {
                mkdir(EXPORTS_DIR, 0755);
        }

        char fname[64];
        if (IRID_ST == IRID_ONE) {
                snprintf(fname, sizeof(fname), "%slx%0.1f_h%.0f-irid1.txt",
                                EXPORTS_DIR, lx, h);
        }
        else if (IRID_ST == IRID_TEN) {
                snprintf(fname, sizeof(fname), "%slx%0.1f_h%.0f-irid10.txt",
                                EXPORTS_DIR, lx, h);
        }
        else {
                snprintf(fname, sizeof(fname),
                                "%slx%0.1f_h%.0f.txt", EXPORTS_DIR, lx, h);
        }

        FILE * out = fopen( fname, "w");
        if ( ! out ) {
                fprintf(stderr, "Cannot create file.\n");
                exit(1);
        }

	for (int i = 0; i < SIDE; ++i) {
		for (int j = 0; j < SIDE; ++j) {
			fprintf(out, "%d ", L[i][j]);
		}
		fprintf(out, "\n");
	}
}

void
dump_cdf(cdf_t* cdf)
{
        printf("probs: [");
        for (int i = 0; i < cdf->len; ++i) {
                printf(" %.2f,", cdf->probs[i]);
        }
        printf("\b ]\n");
}

// Monte Carlo simulation
void
monte_carlo(int** L, int** IRID, float lx, float h)
{
	printf("Computing with params: lx = %0.1f, h = %d\n", lx, (int)h);

	// init probabilities
	cdf_t* cdf = m_init_cdf(lx);
//         dump_cdf(cdf);

	// Monte Carlo simulation
        int x, y;
        Interaction_t evt;
	for (int i = 0; i < RUNS; ++i) {
                // pick a random interaction
		evt = pick_rd_interaction(cdf);
		// pick a random site
		x = rand() % SIDE;
		y = rand() % SIDE;
                // update site state with the selected interaction if conditions
                // are met
		interact(L, IRID, h, x, y, evt);
	}
        save_lattice(L, lx, h);
        free(cdf);
}

// create a new lattice
int**
m_gen_lattice()
{
        // lattice alloc
	int** lattice = malloc(sizeof(int*) * SIDE);
	if ( ! lattice ) {
                memerr("lattice", 1);
	}
	for (int i = 0; i < SIDE; ++i) {
		lattice[i] = malloc(sizeof(int) * SIDE);
		if ( ! lattice[i] ) {
                        memerr("row for lattice", 1);
		}
	}
        return lattice;
}


//// main ~~~
int
main(int argc, char *argv[])
{
        // precompute maximum index in lattice for optimization purpose
	MAX_IDX = SIDE - 1;

	// initialize srand with a seed
	srand(time(NULL));

        // array of parameters to test
//         float lx_vals[4] = { 0.5f, 1.5f, 2.5f, 3.5f };
// 	float h_vals[4] = { 5.f, 15.f, 16.f, 20.f };
	float* lx_vals;
	float* h_vals;
        int lx_vals_sz = 0;
        int h_vals_sz = 0;

        int** L = m_gen_lattice();  // main lattice

	// initialize iridophores lattice
	int** IRID = m_gen_lattice();  // iridophores lattice
	reset_lattice(IRID);

        if (argc == 2) {
                lx_vals = malloc(sizeof(float) * 1);
                if ( ! lx_vals ) {
                        memerr("lx_vals", 1);
                }
                lx_vals[0] = 2.5f;
                lx_vals_sz = 1;

                h_vals = malloc(sizeof(float) * 1);
                if ( ! h_vals ) {
                        memerr("h_vals", 1);
                }
                h_vals[0] = 16.f;
                h_vals_sz = 1;

                // one row of iridophores, centered
                if ( strcmp(argv[1], "--irid1") == 0 ) {
                        IRID_ST = IRID_ONE;
                        for (int j = 0; j < SIDE; ++j) {
                                IRID[(int)SIDE/2][j] = IRIDOPHORE_ID;
                        }
                }
                // 10 rows of iridophores, centered
                else if ( strcmp(argv[1], "--irid10") == 0 ) {
                        IRID_ST = IRID_TEN;
                        for (int i = (SIDE-10)/2; i < (SIDE+10)/2; ++i) {
                                for (int j = 0; j < SIDE; ++j) {
                                        IRID[i][j] = IRIDOPHORE_ID;
                                }
                        }
                }
        }
        else {
                lx_vals = malloc(sizeof(float) * 1);
                if ( ! lx_vals ) {
                        memerr("lx_vals", 1);
                }
                lx_vals[0] = 0.5f;
                lx_vals[1] = 1.5f;
                lx_vals[2] = 2.5f;
                lx_vals[3] = 3.5f;
                lx_vals_sz = 4;

                h_vals = malloc(sizeof(float) * 3);
                if ( ! h_vals ) {
                        memerr("h_vals", 1);
                }
                h_vals[0] = 5.f;
                h_vals[1] = 10.f;
                h_vals[2] = 15.f;
                h_vals[3] = 16.f;
                h_vals_sz = 4;
        }

	// run simulations
	for (int i = 0; i < lx_vals_sz; ++i) {
		for (int j = 0; j < h_vals_sz; ++j) {
                        reset_lattice(L);  // reset lattice before a new run
			monte_carlo(L, IRID, lx_vals[i], h_vals[j]);
		}
	}

        free(L);
        free(IRID);
        free(lx_vals);
        free(h_vals);
	return 0;
}
